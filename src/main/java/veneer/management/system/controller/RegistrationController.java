package veneer.management.system.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import veneer.management.system.model.User;
import veneer.management.system.model.UserTo;
import veneer.management.system.model.authentication.AuthenticationResult;
import veneer.management.system.service.RegistrationService;
import veneer.management.system.service.SecurityService;



@RestController()
public class RegistrationController {

    @Autowired
    private RegistrationService registrationService;

    @Autowired
    private SecurityService securityService;


    @Autowired
    private AuthenticationManager authenticationManager;


    Logger logger = LoggerFactory.getLogger(RegistrationController.class);


    //@GetMapping("/registeruser")
    @PostMapping("/registeruser")
//    @CrossOrigin(origins = "http://localhost:4200")
    public User registerUser(@RequestBody User user) throws Exception {
        String tmpKeyCard = user.getCode();
        if (tmpKeyCard != null && "".equals(tmpKeyCard)) {
            User userObject = registrationService.fetchUserByCode(tmpKeyCard);
            if (userObject != null) {
                throw new Exception("user with such a card is already registered");
            }
        }

        User userObject = null;
        userObject = registrationService.saveUser(user);
        return userObject;


    }


    @PostMapping("/login")
//    @CrossOrigin(origins = "http://localhost:4200")
    public AuthenticationResult authenticate(@RequestBody UserTo userTo)throws Exception { //User loginUser(@RequestBody User user) throws Exception {

        try {
            authenticationManager.authenticate(new  UsernamePasswordAuthenticationToken(userTo.getCode(), userTo.getPassword()));
        } catch (Exception e) {
            throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED, "Bad credentials");
        }


//        String tmpKey = userTo.getCode();
//        String tmpPassword = userTo.getPassword();
//        User userObject =  null;
//
//        if (tmpKey != null && tmpPassword != null) {
//            userObject =  registrationService.fetchUserByKeyCardAndPassword(tmpKey, tmpPassword);
//        }
//        if(userObject== null){
//            throw new Exception("You entered wrong credentials");
//        }
//        System.out.println(tmpKey+ " | "+ tmpPassword);
//
//        return userObject;
        return new AuthenticationResult(securityService.generateToken(securityService.loadUserByUsername(userTo.getCode())));
    }
    //for submitting registration form
    //test comment for commit


}
