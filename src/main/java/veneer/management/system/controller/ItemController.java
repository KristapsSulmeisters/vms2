package veneer.management.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import veneer.management.system.exception.ResourceNotFoundException;
import veneer.management.system.model.Item;
import veneer.management.system.service.ItemService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
//@CrossOrigin(origins = "http://localhost:4200")
public class ItemController {
    @Autowired
    private ItemService itemService;


    @GetMapping("/items")
    public List<Item> getAllItems() {
        return itemService.getAllItems();
    }

    @GetMapping("/items/{id}")
    public ResponseEntity<Item> getItemById(@PathVariable(value = "id") Long itemId)
            throws ResourceNotFoundException {
        Item item = itemService.getItemById(itemId).orElseThrow(()
                ->
                new ResourceNotFoundException("item not found "));


        return ResponseEntity.ok().body(item);
    }

    @PostMapping("/items")
    public Item postItemById(@Valid @RequestBody Item item) {

        return itemService.createItem(item);
    }

    @PutMapping("/items/{id}")
    public ResponseEntity<Item> updateItem(@PathVariable(value = "id") Long itemId,
                                           @Valid @RequestBody Item itemDetails) throws ResourceNotFoundException {
        Item item = itemService.getItemById(itemId).orElseThrow(()->
                new ResourceNotFoundException("no item with such Id"));



        item.setWidth(itemDetails.getWidth());
        item.setName(itemDetails.getName());
        item.setLength(itemDetails.getLength());
        item.setItem_class(itemDetails.getItem_class());
        item.setDescr(itemDetails.getDescr());
        item.setHeight(itemDetails.getHeight());
      //  item//.setDate_modified(itemDetails.getDate_modified());
        final Item updatedItem = itemService.updateItem(itemId, item);
        return ResponseEntity.ok(item);
    }

    @DeleteMapping("/items/{id}")
    public Map<String, Boolean> deleteItem(@PathVariable(value = "id") Long itemId)
            throws ResourceNotFoundException {
        Item item = itemService.getItemById(itemId).orElseThrow(()->
                new ResourceNotFoundException("Item for this id ::" + itemId +" not found" ));

        itemService.deleteItem(itemId);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
