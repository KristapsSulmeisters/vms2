package veneer.management.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import veneer.management.system.exception.ResourceNotFoundException;
import veneer.management.system.model.Pack;
import veneer.management.system.service.PackService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
//@CrossOrigin(origins = "http://localhost:4200")
public class PackController {
    @Autowired
    private PackService packService;


//    @GetMapping("/packs/items")
//    public List<Item> getItems(){
//        return packService.getAllItems();
//    }
//
//    @GetMapping("/packs/machines")
//    public List<Machine> getAllMachines(){
//        return packService.getAllMachines();
//    }
//
//
//    @GetMapping("/packs/users")
//    public List<User> getUsers(){
//        return packService.getAllUsers();
//    }


    @GetMapping("/packs")
    public List<Pack> getPacks() {
        List<Pack> packList = packService.getAllPacks();
        List<Pack> packList2 = packService.getAllPacks();

        String a ="zz";
        return packService.getAllPacks();
    }

    @GetMapping("/packs/{id}")
    public ResponseEntity<Pack> getPackById(@PathVariable(value = "id") Long pId)
            throws ResourceNotFoundException {
        Pack pack = packService.getPackById(pId).orElseThrow(()
                ->
                new ResourceNotFoundException("Pack not found "));


        return ResponseEntity.ok().body(pack);
    }

    @PostMapping("/packs")
    public Pack createPack(@Valid @RequestBody Pack pack) {
         Object o = pack;
        return packService.createPack(pack);
    }

    // TODO: Add filter functionality on packs with from - to


    @PutMapping("/packs/{id}")
    public ResponseEntity<Pack> updatePack(@PathVariable(value = "id") Long pId,
                                           @Valid @RequestBody Pack packDetails) throws ResourceNotFoundException {
        Pack pack = packService.getPackById(pId).orElseThrow(()->
                new ResourceNotFoundException("no pack with such Id"));



        pack.setVolume(packDetails.getVolume());
       // pack.setUser_id(packDetails.getUser_id());
        pack.setQuantity(packDetails.getQuantity());
     //   pack.setFs_p_machine_id(packDetails.getFs_p_machine_id());
        pack.setFs_p_is_active(packDetails.getFs_p_is_active());
       // pack.setItem_id(packDetails.getItem_id());
        //  item//.setDate_modified(itemDetails.getDate_modified());
        final Pack updatedPack = packService.updatePack(pId, pack);
        return ResponseEntity.ok(pack);
    }

    @DeleteMapping("/packs/{id}")
    public Map<String, Boolean> deleteItem(@PathVariable(value = "id") Long packId)
            throws ResourceNotFoundException {
        Pack pack = packService.getPackById(packId).orElseThrow(()->
                new ResourceNotFoundException("Pack for this id ::" + packId +" not found" ));

        packService.deletePack(packId);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
