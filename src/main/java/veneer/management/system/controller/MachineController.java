package veneer.management.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import veneer.management.system.exception.ResourceNotFoundException;
import veneer.management.system.model.Machine;
import veneer.management.system.repos.MachineRepository;

import javax.validation.Valid;
import java.util.List;

//@CrossOrigin(origins = "http://localhost:4200")
@RestController("/machines")
public class MachineController {
    @Autowired
    private MachineRepository machineRepository;

    @GetMapping("/")
    public String welcome(){
        return "Welcome to VMS";
    }


    @GetMapping("/machines")
//    @CrossOrigin(origins = "http://localhost:4200")
    public List<Machine> getAllMachines() {
        List<Machine> listM = machineRepository.findAll();
       // return machineRepository.findAll();
        return listM;
    }

    @GetMapping("/machines/{id}")
//    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Machine> getMachineById(@PathVariable(value = "id") Long machineId)
            throws ResourceNotFoundException {
        Machine machine = machineRepository.findById(machineId)
                .orElseThrow(() -> new ResourceNotFoundException("Machine with specified id not found::" + machineId));
        return ResponseEntity.ok().body(machine);
    }

    @PostMapping("/machines")
//    @CrossOrigin(origins = "http://localhost:4200")
    public Machine createMachine(@Valid @RequestBody Machine machine) {

        return machineRepository.save(machine);
    }

    @PostMapping("/machines/{id}")
//    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Machine> updateMachine(@PathVariable(value = "id") Long machineId,
                                                 @Valid @RequestBody Machine machineDetails) throws ResourceNotFoundException {
        Machine machine = machineRepository.findById(machineId).orElseThrow(() -> new
                ResourceNotFoundException("Machine with this id not found" + machineId));

        machine.setItem_id(machineDetails.getItem_id());
        machine.setName(machineDetails.getName());
        final Machine updatedMachine = machineRepository.save(machine);
        return ResponseEntity.ok(updatedMachine);
    }

    @DeleteMapping("/machines/{id}")
//    @CrossOrigin(origins = "http://localhost:4200")
    public ResponseEntity<Machine> deleteMachine(@PathVariable(value = "id") Long machId)
        throws ResourceNotFoundException{
        Machine machineToDelete =
                machineRepository.findById(machId)
                .orElseThrow(()->
                        new ResourceNotFoundException( "Machine with id " + machId + " not found"));
        machineRepository.delete(machineToDelete);
        return ResponseEntity.ok(machineToDelete);
    }


}
