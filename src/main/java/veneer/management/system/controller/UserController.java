package veneer.management.system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import veneer.management.system.exception.ResourceNotFoundException;
import veneer.management.system.model.User;
import veneer.management.system.service.UserService;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
//@CrossOrigin(origins = "http://localhost:4200")
public class UserController {

    @Autowired
    private UserService userService;


    @GetMapping("/users")
    public List<User> getAllUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/userdetails/{id}")
    public ResponseEntity<User> getUserById(@PathVariable(value = "id") Long userId)
            throws ResourceNotFoundException {
        User user = userService.getUserById(userId).orElseThrow(()
        ->
        new ResourceNotFoundException("user not found "));
        return ResponseEntity.ok().body(user);
    }

    @PostMapping("/users")
    public User postUserById(@Valid @RequestBody User user) {

        return userService.createUser(user);
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<User> updateUser(@PathVariable(value = "id") Long userId,
                                               @Valid @RequestBody User userDetails) throws ResourceNotFoundException {
        User user = userService.getUserById(userId).orElseThrow(()->
                new ResourceNotFoundException("no user with such Id"));



        user.setSurname(userDetails.getSurname());
        user.setCode(userDetails.getCode());
        user.setName(userDetails.getName());
        final User updatedUser = userService.updateUser(userId, user);
        return ResponseEntity.ok(user);
    }

    @DeleteMapping("/users/{id}")
    public Map<String, Boolean> deleteUser(@PathVariable(value = "id") Long userId)
            throws ResourceNotFoundException {
        User user = userService.getUserById(userId).orElseThrow(()->
                new ResourceNotFoundException("User for this id ::" + userId +" not found" ));

        userService.deleteUser(userId);
        Map<String, Boolean> response = new HashMap<>();
        response.put("deleted", Boolean.TRUE);
        return response;
    }
}
