package veneer.management.system.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import veneer.management.system.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
