package veneer.management.system.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import veneer.management.system.model.Machine;

@Repository
public interface MachineRepository  extends JpaRepository<Machine, Long> {
}
