package veneer.management.system.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import veneer.management.system.model.Pack;

@Repository
public interface PackRepository extends JpaRepository<Pack, Long> {
//    public Pack findPackByCreated(Date created);

//    List<Pack> findByDateBetween(Date from, Date to);
}
