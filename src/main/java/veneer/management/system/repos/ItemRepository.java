package veneer.management.system.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import veneer.management.system.model.Item;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {


}
