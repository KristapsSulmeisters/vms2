package veneer.management.system.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import veneer.management.system.model.User;

@Repository
public interface RegistrationRepository extends JpaRepository<User, Long> {

    public User findByCode(String code);

    public User findByCodeAndPassword(String code, String password);
}
