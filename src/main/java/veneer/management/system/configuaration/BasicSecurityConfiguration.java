package veneer.management.system.configuaration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class BasicSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public JwtAuthFilter jwtAuthFilterBean() throws Exception {
        return new JwtAuthFilter(authenticationManagerBean());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
                .csrf().disable().cors().and().headers().frameOptions()
                .sameOrigin() // allow frameOptions for H2 console
                .and().exceptionHandling().and().addFilter(jwtAuthFilterBean())

                .authorizeRequests()
                //.antMatchers("/h2/**").permitAll() // permit all requests to H2 console, they are secured anyway
                .antMatchers(HttpMethod.POST,   "/registeruser").permitAll() // allow creation of users
                .antMatchers(HttpMethod.POST,   "/login").permitAll() // allow login for anonymous user
                .antMatchers(HttpMethod.POST,   "/machines").permitAll()
                .antMatchers(HttpMethod.DELETE, "/machines/*").permitAll()
                .antMatchers(HttpMethod.GET,    "/machines").permitAll()
                .antMatchers(HttpMethod.GET,    "/machines/*").permitAll()
                .antMatchers(HttpMethod.GET,    "/users").permitAll()
                .antMatchers(HttpMethod.POST,   "/users").permitAll()
                .antMatchers(HttpMethod.PUT,    "/users/*").permitAll()
                .antMatchers(HttpMethod.DELETE, "/users/*").permitAll()
                .antMatchers(HttpMethod.GET,    "/userdetails/*").permitAll()
                //items
                .antMatchers(HttpMethod.GET,    "/items").permitAll()
                .antMatchers(HttpMethod.GET,   "/items/*").permitAll()
                .antMatchers(HttpMethod.POST,    "/items").permitAll()
                .antMatchers(HttpMethod.PUT,    "/items/*").permitAll()
                .antMatchers(HttpMethod.DELETE, "/items/*").permitAll()
                //packs
                .antMatchers(HttpMethod.GET,    "/packs").permitAll()
                .antMatchers(HttpMethod.GET,    "/packs/*").permitAll()
                .antMatchers(HttpMethod.DELETE,    "/packs/*").permitAll()
                .antMatchers(HttpMethod.POST,    "/packs").permitAll()
                .antMatchers(HttpMethod.PUT,    "/packs/*").permitAll()
                .antMatchers(HttpMethod.GET,    "/packs/items").permitAll()
                .antMatchers(HttpMethod.GET,    "/packs/machines").permitAll()
                .antMatchers(HttpMethod.GET,    "/packs/users").permitAll()
                .antMatchers(HttpMethod.OPTIONS,"/**").permitAll()
                .anyRequest().authenticated(); // deny all other requests by default


    }
}
