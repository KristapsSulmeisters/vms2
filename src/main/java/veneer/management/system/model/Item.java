package veneer.management.system.model;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false)
    private String name;


    @Column(name = "width", nullable = false)
    private Integer width;

    @Column(name = "length", nullable = false)
    private Integer length;


    @Column(name = "height", nullable = false)
    private Integer height;

    @Column(name = "descr", nullable = true)
    private String descr;

    @Column(name = "item_Class", nullable = false)
    private String item_class;

    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_modified", nullable = false)
    private Date date_modified;


    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "date_created", nullable = false)
    private Date date_created;


  //  @OneToOne(mappedBy = "item")
//    @ManyToOne(optional=false)
//    @JoinColumn(name="pack_id", referencedColumnName="id")
//    private Pack pack;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public Integer getLength() {
        return length;
    }

    public void setLength(Integer length) {
        this.length = length;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getItem_class() {
        return item_class;
    }

    public void setItem_class(String item_Class) {
        this.item_class = item_Class;
    }

    public Date getDate_modified() {
        return date_modified;
    }

    public void setDate_modified() {
        this.date_modified = date_modified;
    }

    public Item() {
    }
}
