package veneer.management.system.model;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Pack {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @CreationTimestamp
    private Date created;
    @Column(name = "quantity", nullable = false)
    private Long quantity;
    @Column(name = "volume", nullable = false)
    private Long volume;

//    @Column(name = "user_id", nullable = false)
//    private Long user_id;

//    @Column(name = "item_id", nullable = false)
//    private Long item_id;

//    @Column(name = "fs_p_machine_id", nullable = false)
//    private Long fs_p_machine_id;

    @ManyToOne(optional = false)
    @JoinColumn(name = "fs_p_machine_id", nullable = false, referencedColumnName = "id")
    private Machine machine;

    @Column(name = "fs_p_is_active", nullable = false)
    private Boolean fs_p_is_active;

    @ManyToOne(optional = false)
    @JoinColumn(name = "Item_id", nullable = false, referencedColumnName = "id")
    private Item item;

    @ManyToOne(optional = false)
    @JoinColumn(name = "User_id", nullable = false, referencedColumnName = "id")
    private User user;





    public Pack() {
    }
    public Item getItem() {
        return item;
    }


    public void setItem(Item item) {
        this.item = item;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getVolume() {
        return volume;
    }

    public void setVolume(Long volume) {
        this.volume = volume;
    }

//    public Long getUser_id() {
//        return user_id;
//    }
//
//    public void setUser_id(Long user_id) {
//        this.user_id = user_id;
//    }

//    public Long getItem_id() {
//        return item_id;
//    }

//    public void setItem_id(Long item_id) {
//        this.item_id = item_id;
//    }

//    public Long getFs_p_machine_id() {
//        return fs_p_machine_id;
//    }

//    public void setFs_p_machine_id(Long fs_p_machine_id) {
//        this.fs_p_machine_id = fs_p_machine_id;
//    }


    public Machine getMachine() {
        return machine;
    }

    public void setMachine(Machine machine) {
        this.machine = machine;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getFs_p_is_active() {
        return fs_p_is_active;
    }

    public void setFs_p_is_active(Boolean fs_p_is_active) {
        this.fs_p_is_active = fs_p_is_active;
    }
}
