package veneer.management.system.model;

import javax.persistence.*;

@Entity
public class Machine {


    private Long    id;

    private String  name;

    private long    item_id;

    public Machine() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long getId() {
        return id;
    }



    public void setItem_id(long item_id) {
        this.item_id = item_id;
    }

    @Column(name = "item_id", nullable = false)
    public long getItem_id() {
        return item_id;
    }

//    public void setItemid(long itemid) {
//        this.itemid = itemid;
//    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name = "name" )
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
