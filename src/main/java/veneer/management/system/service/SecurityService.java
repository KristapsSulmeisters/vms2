package veneer.management.system.service;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import veneer.management.system.model.User;
import veneer.management.system.repos.RegistrationRepository;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class SecurityService implements UserDetailsService {
    @Value("${app.authentication.signature.secret}")
    private String SECRET;

    @Value("${app.authentication.validity.period}")
    private int VALIDITY;

    @Autowired
    private RegistrationRepository registrationRepository;


//    @Override
//    public User loadUserByKeyCardAndPassword(String keycard, String password) throws UsernameNotFoundException {
//        User user = userRepository.findByKeyCardAndPassword(keycard, password);
//        if (user == null) {
//            throw new UsernameNotFoundException(String.format("User \"%s\",\"%s\" not found", keycard, password));
//        }
//        return user;
//    }
//    @Override
//    public User fetchUserByKeyCardAndPassword(String keyCardCode, String password) throws Exception{
//        User user = registrationRepository.findByKeyCardAndPassword(keyCardCode, password);
//        if(user == null){
//            throw new Exception(String.format("User \"%s\",\"%s\" not found", keyCardCode, password));
//        }
//        return registrationRepository.findByKeyCardAndPassword(keyCardCode, password);
//
//    }

    public String generateToken(User user) {
        Map<String, Object> claims = new HashMap<>();
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(user.getId() + "")
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + VALIDITY))
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
    }

    public long getUserId() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return Long.parseLong((String)authentication.getPrincipal());
    }

    @Override
    public User loadUserByUsername(String code) throws UsernameNotFoundException {
        User user = registrationRepository.findByCode(code);
        if(user == null){
            throw new UsernameNotFoundException(String.format("User \"%s\" not found", code));
        }
        return user;
//        return registrationRepository.findByKeyCardAndPassword(keyCode, password);
    }
}
