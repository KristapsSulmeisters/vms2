package veneer.management.system.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import veneer.management.system.model.User;
import veneer.management.system.repos.RegistrationRepository;

@Service
public class RegistrationService {

    @Autowired
    private RegistrationRepository registrationRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public User saveUser(User user){
        final User userEntity = new User();
            userEntity.setCode(user.getCode());
            userEntity.setId(user.getId());
            userEntity.setName(user.getName());
            userEntity.setPassword(passwordEncoder.encode(user.getPassword()));
            userEntity.setSurname(user.getSurname());

       return registrationRepository.save(userEntity);
    }

    public User fetchUserByCode(String code){
        return registrationRepository.findByCode(code);

    }

    public User fetchUserByKeyCardAndPassword(String keyCardCode, String password){
        return registrationRepository.findByCodeAndPassword(keyCardCode, password);

    }
}
