package veneer.management.system.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import veneer.management.system.model.Item;
import veneer.management.system.model.Pack;
import veneer.management.system.repos.ItemRepository;
import veneer.management.system.repos.MachineRepository;
import veneer.management.system.repos.PackRepository;
import veneer.management.system.repos.UserRepository;

import java.util.List;
import java.util.Optional;

@Service
public class PackService {
    @Autowired
    private PackRepository packRepo;

    @Autowired
    private ItemRepository itemRepo;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MachineRepository machineRepository;



//    public List<Item> getAllItems()
//    {
//        return (List<Item>) itemRepo.findAll();
//    }
//
//    public List<User> getAllUsers(){
//        return (List<User>) userRepository.findAll();
//    }
//    public List<Machine> getAllMachines(){
//        return (List<Machine>) machineRepository.findAll();
//    }

    public List<Pack> getAllPacks() {
        return packRepo.findAll();
    }


    public Optional<Pack> getPackById(Long packId){

        return  packRepo.findById(packId);
    }

    //TODO: CREATE filter for packs
//    public List<Pack> filterPack(Date from, Date to){
//        return packRepo.findByDateBetween(  from, to );
//    }

    public Pack createPack( Pack pack) {

        return packRepo.save(pack);
    }

    public List<Item> getItems(){
        return itemRepo.findAll();
    }

    public Pack updatePack( Long pId,  Pack pack )  {
        Pack pToEdit = packRepo.getOne(pId);
        //itemToEdit.setDate_modified(item.getDate_modified());
        pToEdit.setFs_p_is_active(pack.getFs_p_is_active());
       // pToEdit.setFs_p_machine_id(pack.getFs_p_machine_id());
     //   pToEdit.setItem_id(pack.getItem_id());
        pToEdit.setQuantity(pack.getQuantity());
      //  pToEdit.setUser_id(pack.getUser_id());
        pToEdit.setVolume(pack.getVolume());



        return packRepo.save(pToEdit);
    }


    public void deletePack(Long pId)
    {
        Pack pack = packRepo.getOne(pId);

        packRepo.delete(pack);

    }

}
