package veneer.management.system.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import veneer.management.system.model.User;
import veneer.management.system.repos.UserRepository;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;



    public List<User> getAllUsers() {
        return userRepository.findAll();
    }


    public Optional<User> getUserById(Long userId){

        return   userRepository.findById(userId);
    }


    public User createUser( User user) {
        return userRepository.save(user);
    }


    public User updateUser( Long userId, User user )  {
         User userToEdit = userRepository.getOne(userId);
         userToEdit.setCode(user.getCode());
         userToEdit.setSurname(user.getSurname());

         return userRepository.save(userToEdit);
    }


    public void deleteUser(Long userId)
    {
        User user = userRepository.getOne(userId);


        userRepository.delete(user);

    }
}
