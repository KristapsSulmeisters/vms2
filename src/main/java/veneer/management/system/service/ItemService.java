package veneer.management.system.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import veneer.management.system.model.Item;

import veneer.management.system.repos.ItemRepository;

import java.util.List;
import java.util.Optional;

@Service
public class ItemService {
    @Autowired
    private ItemRepository itemRepository;

    public List<Item> getAllItems() {
        return itemRepository.findAll();
    }


    public Optional<Item> getItemById(Long itemId){

        return   itemRepository.findById(itemId);
    }


    public Item createItem( Item item) {
        return itemRepository.save(item);
    }


    public Item updateItem( Long itId, Item item )  {
        Item itemToEdit = itemRepository.getOne(itId);
        //itemToEdit.setDate_modified(item.getDate_modified());
        itemToEdit.setDescr(item.getDescr());
        itemToEdit.setHeight(item.getHeight());
        itemToEdit.setItem_class(item.getItem_class());
        itemToEdit.setLength(item.getLength());
        itemToEdit.setName(item.getName());
        itemToEdit.setWidth(item.getWidth());


        return itemRepository.save(itemToEdit);
    }


    public void deleteItem(Long itemId)
    {
        Item item = itemRepository.getOne(itemId);

        itemRepository.delete(item);

    }

}
